package com.tomtom.dedup.test

import akka.actor.ActorSystem
import akka.actor.Props
import akka.testkit.ImplicitSender
import akka.testkit.TestActorRef
import akka.testkit.TestKit
import akka.testkit.TestProbe
import java.io.StringWriter
import java.io.Writer
import org.scalatest.WordSpecLike
import org.scalatest.Matchers._
import scala.concurrent.duration._
import com.tomtom.dedup._

class ReducerMock(override val writer: Writer) extends Reducer

class ReducerActorSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with WordSpecLike {

  def this() = this(ActorSystem("ReducerActorSpec"))

  "Reducer Actor" should {
    "write a second uniq line as well" in {
      val reducer = TestActorRef(new ReducerMock(new StringWriter))
      val stringWriter = reducer.underlyingActor.writer

      reducer ! Response(1, "a", duplicate = false)
      reducer ! Response(2, "b", duplicate = false)

      stringWriter.toString should include("a")
      stringWriter.toString should include("b")
    }

    "write lines only Once" in {

      val reducer = TestActorRef(new ReducerMock(new StringWriter))
      val stringWriter = reducer.underlyingActor.writer

      reducer ! Response(1, "a", duplicate = false)
      reducer ! Response(2, "b", duplicate = false)
      reducer ! Response(3, "a", duplicate = true)

      stringWriter.toString should include("a")
      stringWriter.toString should include("b")
      stringWriter.toString should (not include ("b\na"))
    }
  }

  "wait for future lines" in {
    val reducer = TestActorRef(new ReducerMock(new StringWriter))
    val stringWriter = reducer.underlyingActor.writer

    reducer ! Response(1, "a", duplicate = false)
    reducer ! Response(3, "b", duplicate = false)

    stringWriter.toString should include("a")
    stringWriter.toString should (not include("b"))

  }

  "wait for future lines and write them once they arrived" in {
    val reducer = TestActorRef(new ReducerMock(new StringWriter))
    val stringWriter = reducer.underlyingActor.writer

    reducer ! Response(1, "a", duplicate = false)
    reducer ! Response(3, "b", duplicate = false)

    stringWriter.toString should include("a")
    stringWriter.toString should (not include("b"))

    reducer ! Response(2, "b", duplicate = false)

    stringWriter.toString should include("b")
    stringWriter.toString should (not include("c"))

  }

 "not wait too long for delayed lines " in {
    val reducer = TestActorRef(new ReducerMock(new StringWriter))
    val stringWriter = reducer.underlyingActor.writer

    reducer ! Response(1, "x", duplicate = false)
    reducer ! Response(3, "y", duplicate = false)

    stringWriter.toString should include("x")
    stringWriter.toString should (not include("y"))
   
   //sorry for this
    Thread.sleep(200)
    stringWriter.toString should include("y")
  }

  "kill itself once eof reached" in {

    val reducer = TestActorRef(new ReducerMock(new StringWriter))
    val stringWriter = reducer.underlyingActor.writer

    val probe = TestProbe()
    probe watch reducer

    reducer ! Response(1, "x", duplicate = false)
    reducer ! EndOfFile(1)

    stringWriter.toString should include("x")
    probe.expectTerminated(reducer)        
  }  
  
}
