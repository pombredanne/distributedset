package com.tomtom.dedup

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.routing.ConsistentHashingRouter
import java.io.BufferedReader
import java.io.FileReader
import scala.io.Source

object Main extends App {


  val system = ActorSystem("deduplicate")
  val inputPath = args(0)
  val outputPath = args(1)
  val set = system.actorOf(Props(classOf[SetActor]).withRouter(ConsistentHashingRouter(10)), name = "setRouter")
  val reducer= system.actorOf(Props(classOf[ReducerImp], outputPath), "reducer")
  val iterator = Source.fromFile(inputPath).getLines
  var lineNumber = 0

  while(iterator.hasNext){
    lineNumber += 1
    set.tell( Request(lineNumber, iterator.next()), reducer)
  }
  
  reducer ! EndOfFile(lineNumber)
  
}










